# Pastanudelbook

Pastanudelbook ist eine mobile Anwendung, die es Benutzern ermöglicht, Rezepte für verschiedene Nudelgerichte zu durchsuchen, zu speichern und zu teilen. Die App bietet eine Vielzahl von Funktionen, darunter:

- Anmeldung und Authentifizierung für Benutzer
- Suche nach Nudelrezepten nach verschiedenen Kriterien wie Zutaten, Zubereitungszeit usw.
- Favoritenliste zum Speichern von Lieblingsrezepten
- Die Möglichkeit, Rezepte mit Freunden über verschiedene soziale Medien zu teilen

## Installation

Um das Projekt lokal auszuführen, müssen Sie die folgenden Schritte ausführen:

1. Klonen Sie das Git-Repository auf Ihren lokalen Computer:

```bash
git clone https://gitlab.com/Mahmodhota/Pastanudelbook.git

